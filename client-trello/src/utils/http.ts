export function request (method: 'Get' | 'Post', url: string, data?: any) {
  return new Promise <any>((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(method, url)
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.onload = () => {
      try {
        resolve(JSON.parse(xhr.responseText))
      } catch (err) {
        reject(err)
      }
    }
    xhr.onerror = reject
    xhr.send(JSON.stringify(data))
  })
}
export function get (url: string) {
  return request('Get', url)
}
export function post (url: string, data: any) {
  return request('Post', url, data)
}
