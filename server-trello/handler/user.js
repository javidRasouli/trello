'use strict';
const jwt = require('jsonwebtoken');
const { insertOne } = require("../db")

const login = async (req, res) => {
    try {
        if (!req.body.username) {
            res.status(400).json({ success: false, error: "missing username" })
            return
        }
        if (req.body.username.length < 3) {
            res.status(400).json({ success: false, error: "invalid username" })
        }
        if (!req.body.pass || req.body.pass.length < 8) {
            res.status(400).json({ success: false, error: "invalid pass" })
            return
        }
        const db = await getInstance()
        const users = db.collection("users")
        const user = await users.findOne({ username: req.body.username, pass: req.body.pass })
        if (!user) {
            res.status(403).json({ success: false })
            return
        }
        const ok = await generateToken(res, user._id)
        res.json({ success: ok })
    } catch (err) {
        console.log(err)
        res.status(500).json({ success: false })
    }
}
const register = async (req, res) => {
    const users = 'users'
    insertOne(users,req.body)
}

const refreshToken = async (req, res) => {
    try {
        const token = req.cookies.refresh_token;
        if (!token) {
            console.error("token not found")
            res.status(401).json({ success: false })
            return
        }
        const decoded = jwt.verify(token, "40bil");
        const ok = await generateToken(res, decoded.id)
        res.json({ success: ok })

    } catch (err) {
        console.log(err)
        res.status(500).json({ success: false })
    }
}


const check = (req, res) => {
    res.json({success:true})
}




module.exports = { login, register, refreshToken, check } 