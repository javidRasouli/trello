"use strict"
const { MongoClient } = require("mongodb")
const uri = "mongodb://127.0.0.1:27017"
const client = new MongoClient(uri, { useUnifiedTopology: true })

let db
async function initDB() {
    try {
        await client.connect()
        const newDB = client.db("farawin")
        return newDB
        //2: db = client.db("farawin")
    } catch (error) {
        console.log(error)
    }
}

async function getInstance() {
    if (!db) {
        db = await initDB()
        //2: await initDB()
    }
    return db
}
const generateToken = async (res,userID) => {
    const accessToken = jwt.sign({ id: userID }, '30bil',{ expiresIn:"15min" });
    const rToken = jwt.sign({ id: userID }, '40bil', { expiresIn: "10d" });

    const db = await getInstance()
    const users = db.collection("users")
    const ok = await users.updateOne({ _id: userID }, { $set: { refreshToken: rToken }})
    if (!ok){
        res.status(500)
        return false
    }
    res.cookie('access_token', accessToken, { maxAge: 1000 * 60 * 15, httpOnly: true });
    res.cookie('refresh_token', rToken, { maxAge: 3600 * 1000 * 10, httpOnly: true });
    return true
}
async function insertOne(col,insertObject) {
    if(insertObject){
        
            if (!req.body.username) {
                res.status(400).json({ success: false, error: "missing username" })
                return
            }
            if (req.body.username.length < 3) {
                res.status(400).json({ success: false, error: "invalid username" })
            }
            if (!req.body.pass || req.body.pass.length < 8 || !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(req.body.pass)) {
                res.status(400).json({ success: false, error: "invalid pass" })
                return
            }
        }
            try{
                const db = await getInstance()
                const coll = db.collection(col)
                const ok = await coll.insertOne(insertObject)
                if (!ok) {
                    res.status(500).json({ success: false })
                    return
                }
                console.log("INFO:", "insert ok")
                res.json({ success: true })
            } catch (err) {
                console.log(err)
                res.status(500).json({ success: false })
            }
        }
           
    

module.exports = { getInstance,insertOne}